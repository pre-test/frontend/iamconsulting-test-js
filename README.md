# I AM Consulting - Test

> My Test javascript for vuePress design

## Build Setup

``` bash
** Windows **
# install project
$ npm install -g vuepress

# run project
$ cd project-files
$ vuepress dev

** macOS **
# install project
$ sudo npm install -g vuepress

#run project
$ cd project-files
$ sudo vuepress dev