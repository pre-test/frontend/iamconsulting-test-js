module.exports = {
  title: 'I AM Consulting - Test',
  description: 'test for iamconsulting',
  themeConfig: {
    nav: [
      { text: 'Home', link: '/' },
      { text: 'Analysis', link: '/analysis/' }
    ],
    sidebar: ['/', '/analysis/']
  }
}